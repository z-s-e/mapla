# Mapla - Example playback streaming server for the Advanced Linux Playback Engine

See the [Advanced Linux Playback Engine](https://gitlab.com/z-s-e/alpe) project page for general information.


## Installing

Currently the app is packaged only for [Arch AUR](https://aur.archlinux.org/packages/mapla). For other systems you need to build it yourself.


## Dependencies and building

Required packages are Alsa, as well as git, cmake and pkg-config for fetching and building. Everything else is bundled as a git submodule.

Debian/Ubuntu package list:

```bash
sudo apt install -y g++ gcc libc-dev git cmake make pkg-config libasound2-dev
```

Arch package list:

```bash
sudo pacman -Syu --noconfirm --needed base-devel git cmake alsa-lib
```

For a local user install I use the following steps (for that make sure your `PATH` env contains `$HOME/.local/bin`):

```
git clone https://gitlab.com/z-s-e/mapla.git
cd mapla
git submodule update --init
cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX="$HOME/.local" -B build -S .
cmake --build build --target install/strip
```
