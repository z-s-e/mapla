/* Copyright 2024 Zeno Sebastian Endemann <zeno.endemann@mailbox.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <alpe_thread.h>
#include <cstdlib>
#include <lbu/fd_stream.h>
#include <lbu/math.h>
#include <lbu/poll.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <sys/socket.h>
#include <unistd.h>
#include <vector>


static const char EnvAlsaDev[] = "MAPLA_ALSA_DEV";
static const char EnvPort[] = "MAPLA_PORT";
static const char EnvStartupFile[] = "MAPLA_STARTUP";

static const uint16_t DefaultRootPort = 671;
static const uint16_t DefaultUserPort = 19671;


[[noreturn]] static void die(const char* context, const char* message) {
    fprintf(stderr, "ERROR: %s - %s\n", context, message);
    std::exit(EXIT_FAILURE);
};

static int setup_listen_socket(uint16_t port)
{
    int sock = socket(PF_INET, SOCK_STREAM | SOCK_NONBLOCK, 0);
    if( sock < 0 )
        die("create socket", std::strerror(errno));

    int tmp = 1;
    setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &tmp, sizeof(tmp));

    struct sockaddr_in name;
    name.sin_family = AF_INET;
    name.sin_port = htons(port);
    name.sin_addr.s_addr = htonl(INADDR_ANY);
    if( bind(sock, reinterpret_cast<struct sockaddr*>(&name), sizeof(name)) < 0 )
        die("bind socket", std::strerror(errno));
    if( listen(sock, 1) != 0 )
        die("listen socket", std::strerror(errno));
    socklen_t sock_length = sizeof(name);
    if( getsockname(sock, reinterpret_cast<struct sockaddr*>(&name), &sock_length) < 0 )
        die("getsockname", std::strerror(errno));

    printf("Listening on port %d\n", int(ntohs(name.sin_port)));

    return sock;
}

static std::vector<char> read_file(const char *path)
{
    std::vector<char> result;
    if( path == nullptr )
        return {};

    auto f = fopen(path, "rb");
    if( f == nullptr )
        return {};
    struct file_cleanup {
        FILE* file;
        ~file_cleanup() { fclose(file); }
    } cleanup { f };

    fseek(f, 0L, SEEK_END);
    const long s = ftell(f);
    if( s <= 0 )
        return {};

    result.resize(size_t(s));
    fseek(f, 0L, SEEK_SET);
    if( fread(result.data(), size_t(s), 1, f) != 1 )
        return {};
    return result;
}

static void play_raw_s16le_2ch_44100_file(snd_pcm_t* pcm_device, const std::vector<char>& data)
{
    size_t src_pos = 0;
    auto alsa_alpe = alpe::create_alsa(pcm_device);

    if( auto c = alsa_alpe->configure({SND_PCM_FORMAT_S16_LE, 2, 44100}); std::holds_alternative<alpe::error_event>(c) ) {
        return;
    } else {
        auto cfg = alsa_alpe->current_configuration();
        if( cfg.format.type != SND_PCM_FORMAT_S16_LE || cfg.format.rate != 44100 )
            return;
    }

    std::vector<pollfd> pfds;
    {
        auto inst_pfds = alsa_alpe->get_poll_fds();
        pfds.insert(pfds.end(), inst_pfds.begin(), inst_pfds.end());
    }
    auto buffer_appender = [&data, &src_pos, &alsa_alpe] () {
        auto buf = alsa_alpe->get_buffer().array_static_cast<char>();
        if( ! buf )
            return false;
        buf = buf.sub_first(data.size() - src_pos);
        std::memcpy(buf.data(), data.data() + src_pos, buf.size());
        src_pos += buf.size();
        alsa_alpe->advance_buffer(buf.size() / 4, src_pos == data.size() ? alpe::EndOfStream::Yes : alpe::EndOfStream::No);
        return true;
    };
    buffer_appender();
    alsa_alpe->fade_in({});

    while( alsa_alpe->state() == alpe::State::Playing ) {
        while( buffer_appender() )
            ;
        if( ! alsa_alpe->has_buffered_events() )
            lbu::poll::wait_for_events(pfds.data(), pfds.size());
        alsa_alpe->next_event();
    }
}

struct mapla_system_preset : public alpe::engine_parameter_system_preset {
    alpe::engine_parameter generate(alpe::configuration current) override
    {
        alpe::engine_parameter params;
        const auto buffer_frames = alpe::frames_for_time(std::chrono::seconds(4), current.format.rate);
        const auto hw_buffer = (current.period_buffer_count - 1) * current.period_frames;
        params.additional_buffer_count = hw_buffer < buffer_frames ? lbu::idiv_ceil(buffer_frames - hw_buffer, current.period_frames) : 0;
        params.pre_underrun_trigger_frames = alpe::frames_for_time(std::chrono::milliseconds(150), current.format.rate);
        params.rewind_safeguard = alpe::frames_for_time(std::chrono::milliseconds(100), current.format.rate);
        params.emergency_pause_fade = alpe::frames_for_time(std::chrono::milliseconds(10), current.format.rate);
        return params;
    }
};

int main(int, char **)
{
    {
        struct sigaction act = {};
        act.sa_handler = SIG_IGN;
        sigemptyset(&act.sa_mask);
        act.sa_flags = 0;
        if( sigaction(SIGPIPE, &act, nullptr) != 0 )
            die("sigaction", std::strerror(errno));
    }

    if( int err = alpe_thread::current_thread_set_realtime_priority(); err != 0 )
        fprintf(stderr, "Warning: could not set thread realtime priority: %s\n", std::strerror(err));

    uint16_t port = (getuid() == 0 ? DefaultRootPort : DefaultUserPort);
    if( const char* port_env = std::getenv(EnvPort); port_env )
        port = std::atoi(port_env);
    pollfd sock = {setup_listen_socket(port), POLLIN, 0};

    char in_buffer[128];
    char out_buffer[128];
    lbu::stream::fd_input_stream in(lbu::array_ref<char>{in_buffer});
    lbu::stream::fd_output_stream out(lbu::array_ref<char>{out_buffer});
    lbu::unique_fd client;
    lbu::unique_fd last_client;

    lbu::alsa::pcm_device alsa;
    {
        const char* alsa_dev = std::getenv(EnvAlsaDev);
        if( alsa_dev == nullptr )
            alsa_dev = "default";
        if( int err = alsa.open(alsa_dev, SND_PCM_STREAM_PLAYBACK); err < 0 )
            die("alsa open", snd_strerror(err));
        printf("Opened ALSA device '%s'\n", alsa_dev);
    }

    fflush(stdout);

    if( const auto startup = read_file(std::getenv(EnvStartupFile)); ! startup.empty() )
        play_raw_s16le_2ch_44100_file(alsa, startup);

    while( true ) {
        lbu::poll::wait_for_event(sock);

        {
            struct sockaddr_in clientname;
            unsigned size = sizeof(clientname);
            client.reset(lbu::fd(accept4(sock.fd, reinterpret_cast<struct sockaddr*>(&clientname), &size, SOCK_NONBLOCK)));
            if( ! client )
                continue;
            in.set_descriptor(*client, lbu::stream::FdBlockingState::AlwaysNonBlocking);
            out.set_descriptor(*client, lbu::stream::FdBlockingState::AlwaysNonBlocking);

            int one = 1;
            setsockopt(client.get().value, SOL_TCP, TCP_NODELAY, &one, sizeof(one));
        }

        auto alsa_alpe = alpe::create_alsa(alsa);
        mapla_system_preset preset;
        alpe::proxy_run(&in, &out, alsa_alpe.get(), sock, &preset);

        if( in.has_error() ) {
            fprintf(stderr, "Warning: socket stream error: %s\n", std::strerror(in.status()));
        } else {
            shutdown(client.get().value, SHUT_RDWR);
            last_client = std::move(client);
        }

        snd_pcm_drop(alsa);
    }
}
